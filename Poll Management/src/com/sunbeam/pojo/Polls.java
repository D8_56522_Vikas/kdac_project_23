package com.sunbeam.pojo;

import java.sql.Date;

public class Polls {
	private int id;
	private String title;
	private Date start_datetime;
	private Date end_datetime;
	private int created_by;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStart_datetime() {
		return start_datetime;
	}
	public void setStart_datetime(Date start_datetime) {
		this.start_datetime = start_datetime;
	}
	public Date getEnd_datetime() {
		return end_datetime;
	}
	public void setEnd_datetime(Date end_datetime) {
		this.end_datetime = end_datetime;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	
	
	public Polls() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Polls(int id, String title, Date start_datetime, Date end_datetime, int created_by) {
		super();
		this.id = id;
		this.title = title;
		this.start_datetime = start_datetime;
		this.end_datetime = end_datetime;
		this.created_by = created_by;
	}
	@Override
	public String toString() {
		return "Polls [id=" + id + ", title=" + title + ", start_datetime=" + start_datetime + ", end_datetime="
				+ end_datetime + ", created_by=" + created_by + "]";
	}

	
}

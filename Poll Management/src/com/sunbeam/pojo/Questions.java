package com.sunbeam.pojo;

public class Questions {

	private int id;
	private String question_text;
	private int option_A;
	private int option_B;
	private int option_C;
	private int option_D;
	private int poll_id;
	private int question_serial_no;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestion_text() {
		return question_text;
	}
	public void setQuestion_text(String question_text) {
		this.question_text = question_text;
	}
	public int getOption_A() {
		return option_A;
	}
	public void setOption_A(int option_A) {
		this.option_A = option_A;
	}
	public int getOption_B() {
		return option_B;
	}
	public void setOption_B(int option_B) {
		this.option_B = option_B;
	}
	public int getOption_C() {
		return option_C;
	}
	public void setOption_C(int option_C) {
		this.option_C = option_C;
	}
	public int getOption_D() {
		return option_D;
	}
	public void setOption_D(int option_D) {
		this.option_D = option_D;
	}
	public int getPoll_id() {
		return poll_id;
	}
	public void setPoll_id(int poll_id) {
		this.poll_id = poll_id;
	}
	public int getQuestion_serial_no() {
		return question_serial_no;
	}
	public void setQuestion_serial_no(int question_serial_no) {
		this.question_serial_no = question_serial_no;
	}
	public Questions(int id, String question_text, int option_A, int option_B, int option_C, int option_D, int poll_id,
			int question_serial_no) {
		super();
		this.id = id;
		this.question_text = question_text;
		this.option_A = option_A;
		this.option_B = option_B;
		this.option_C = option_C;
		this.option_D = option_D;
		this.poll_id = poll_id;
		this.question_serial_no = question_serial_no;
	}
	public Questions() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Questions [id=" + id + ", question_text=" + question_text + ", option_A=" + option_A + ", option_B="
				+ option_B + ", option_C=" + option_C + ", option_D=" + option_D + ", poll_id=" + poll_id
				+ ", question_serial_no=" + question_serial_no + "]";
	}
	
	
}

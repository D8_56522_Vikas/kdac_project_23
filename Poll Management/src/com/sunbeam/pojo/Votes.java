package com.sunbeam.pojo;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Votes {
	
	private int id ;
	private  int que_id;
	private int user_id;
	private Timestamp answered_datetime ;
	public static SimpleDateFormat sdf;
	static
	{
		sdf=new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQue_id() {
		return que_id;
	}
	public void setQue_id(int que_id) {
		this.que_id = que_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public Timestamp getAnswered_datetime() {
		return answered_datetime;
	}
	public void setAnswered_datetime(Timestamp answered_datetime) {
		this.answered_datetime = answered_datetime;
	}
	public Votes() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Votes(int id, int que_id, int user_id, Timestamp answered_datetime) {
		super();
		this.id = id;
		this.que_id = que_id;
		this.user_id = user_id;
		this.answered_datetime = answered_datetime;
	}
	@Override
	public String toString() {
		return "Votes [id=" + id + ", que_id=" + que_id + ", user_id=" + user_id + ", answered_datetime="
				+ answered_datetime + "]";
	}
	
	
	
	

}

package com.sunbeam.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.sunbeam.pojo.Polls;

public interface IPollOperation {
	

		public String CreatePoll(Polls poll) throws SQLException;
		
		public List<Polls> DisplayPolls(int id) throws SQLException;
		
		public String EditPoll(int id,String title,Timestamp end_datetime) throws SQLException;
		
		public String DeletePoll(int id) throws SQLException;
		
	

}

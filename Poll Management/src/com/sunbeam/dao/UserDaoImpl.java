package com.sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sunbeam.pojo.Users;

import static utils.dbUtil.fetchConnection;




public class UserDaoImpl implements IUserDao {

	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4,pst5;
	
	public UserDaoImpl()throws ClassNotFoundException, SQLException 
	{
		cn=fetchConnection();
		String sql = "insert into users values(default,?,?,?,?,?,?,?)" ;
		String sql1="Select * from users where id=?";
		String sql2="update users set name=?,address=? where id=?";
		String sql3="select * from users where email=? && password=? ";
		String sql4="Update users set password=? where email=?";
		pst2=cn.prepareStatement(sql);
		pst1=cn.prepareStatement(sql1);
		pst3=cn.prepareStatement(sql2);
		pst4=cn.prepareStatement(sql3);
		pst5=cn.prepareStatement(sql4);

		
	}
	
	@Override
	public String signUp(Users newUser) throws SQLException {
		
	
		pst2.setString(1, newUser.getEmail());// 
		pst2.setString(2, newUser.getPassword());//
		pst2.setString(3, newUser.getMobile());// 
		pst2.setString(4, newUser.getName());// 
		pst2.setString(5, newUser.getGender());
		pst2.setString(6, newUser.getAddress());
		pst2.setDate(7, newUser.getBirth_date());


		// query exec : Method of PST : public int executeUpdate() throws SQLExc
		int updateCount = pst2.executeUpdate();
		if (updateCount == 1)
			return "SignUp sucess!!!!";

		return "SignUp failed...";
	
	}
	

	@Override
	public List<Users> gtUserById(int id) throws SQLException {
		List<Users> user=new ArrayList<>(); 
	
		pst1.setInt(1, id);
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				user.add(new Users(rst.getInt(1), rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(6), rst.getString(7), rst.getDate(8)));

		return user;
	}
	
}

	@Override
	public String editProfile(int id, String name, String address) throws SQLException  {
		pst3.setString(1, name);// dept
		pst3.setString(2, address);// sal incr
		pst3.setInt(3, id);
		// exec update : DML
		int updateCount = pst3.executeUpdate();
		if (updateCount == 1)
			return "User details updated....";
		return "Updating User details failed...";
	}

//	@Override
//	public String changePassword(String email, String password) throws SQLException {
//		List<Users> user=new ArrayList<>();
//		pst4.setString(1,email);
//		pst4.setString(2,password);
//		try (ResultSet rst = pst1.executeQuery()) {
//			while (rst.next())
//				user.add(new Users(rst.getInt(1), rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(6), rst.getString(7), rst.getDate(8)));
//				
//	}
	
	
	
	
	
  }

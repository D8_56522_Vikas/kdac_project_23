package com.sunbeam.dao;

import java.sql.SQLException;
import java.util.List;

import com.sunbeam.pojo.Users;

public interface IUserDao {
	
public String signUp(Users newUser)throws SQLException;

public List<Users> gtUserById(int id) throws SQLException ;

public String editProfile(int id, String name,String address) throws SQLException;

//public String changePassword(String email,String password) throws SQLException;

}


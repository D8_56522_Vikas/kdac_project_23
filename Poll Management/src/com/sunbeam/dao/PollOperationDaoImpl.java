package com.sunbeam.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import com.sunbeam.pojo.Polls;
import static utils.dbUtil.fetchConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
public class PollOperationDaoImpl implements IPollOperation {

	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4;
	public PollOperationDaoImpl() throws ClassNotFoundException, SQLException {
		
		cn = fetchConnection();
		String sql = "insert into poll values(default,?,?,?,?)";
		pst1 = cn.prepareStatement(sql);
		String sql2 = "select *from polls where id =?";
		pst2 = cn.prepareStatement(sql2);
		String sql3 = "update polls set title=?,end_datetime=? where id=?,";
		pst3 = cn.prepareStatement(sql3);
		String sql4 = "delete from polls where id=?";
		pst4=cn.prepareStatement(sql4);
		System.out.println("emp dao created...");

	}
	
	@Override
	public String CreatePoll(Polls poll) throws SQLException {
		pst1.setString(1, poll.getTitle());
		pst1.setDate(2, (poll.getStart_datetime()));
		pst1.setDate(3, (poll.getEnd_datetime()));
		pst1.setInt(4, (poll.getCreated_by()));
		
		int updateCount = pst1.executeUpdate();
		if (updateCount == 1)
			return "polls details added....";

		return "Adding emp details failed...";

	}

	@Override
	public List<Polls> DisplayPolls(int id) throws SQLException {
		List<Polls> polls = new ArrayList<>();
		
		pst2.setInt(1, id);
		
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				polls.add(new Polls(rst.getInt(1), rst.getString(2), rst.getDate(3), rst.getDate(4),rst.getInt(id)));

		} 
		return polls;
		
		
	}

	@Override
	public String EditPoll(int id,String title,Timestamp end_datetime) throws SQLException {
		pst3.setString(1, title);
		pst3.setTimestamp(2, end_datetime);
		pst3.setInt(3, id);
		
		int updateCount = pst3.executeUpdate();
		if (updateCount == 1)
			return "poll details updated....";
		return "Updating emp details failed...";
		
	}

	@Override
	public String DeletePoll(int id) throws SQLException {
		
		pst4.setInt(1, id);
		
		int updateCount=pst4.executeUpdate();
		if (updateCount == 1)
			return "Polls details deleted....";
		return "Deleting Polls details failed...";
	}
	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (pst4 != null)
			pst4.close();
		if (cn != null)
			cn.close();
		System.out.println("emp dao cleaned up !");
	}

}
